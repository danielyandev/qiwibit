<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::any('/logout', 'Auth\LoginController@logout')->name('logout');
Route::redirect('/home', '/index')->name('home');

Route::any('/exchange/from/{from}/to/{to}', 'ExchangeController@applyExchange')
    ->where('from', '[\w_]+')
    ->where('to', '[\w_]+')
    ->name('change-action');

Route::get('/exchange/{id}_{email}', 'ExchangeController@verifyExchange')
    ->where('id', '\d+')
    ->name('exchange-verify');

Route::any('/exchange/discard/{id}_{email}', 'ExchangeController@discardExchange')
    ->where('id', '\d+')
    ->name('exchange-discard');
