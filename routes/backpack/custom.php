<?php

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {

    Route::get('dashboard', 'AdminController@dashboard')->name('backpack.dashboard');
    Route::get('/', 'AdminController@redirect')->name('backpack');
    Route::post('/settings_set', 'AdminController@settingsSet');

    Route::get('qiwi_wallets/{wallet}/history', 'QiwiWalletCrudController@showHistory');
    Route::get('qiwi_wallets/{wallet}/refresh', 'QiwiWalletCrudController@refreshBalance');
    Route::get('qiwi_wallets/{wallet}/send', 'QiwiWalletCrudController@showSendForm');
    Route::post('qiwi_wallets/{wallet}/send', 'QiwiWalletCrudController@send');
    CRUD::resource('qiwi_wallets', 'QiwiWalletCrudController');

    Route::get('bitcoin_wallet', 'BitcoinWalletController@history');
    Route::get('bitcoin_wallet/send', 'BitcoinWalletController@showSendForm');
    Route::post('bitcoin_wallet/send', 'BitcoinWalletController@sendBitCoins');

    Route::get('exchange/{exchange}/fail', 'ExchangeCrudController@failExchange');
    CRUD::resource('exchange', 'ExchangeCrudController');
});
