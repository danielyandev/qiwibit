# Обменник
Сделан по заказу на laravel.
Официальную документацию можно найти на [laravel.com](https://laravel.com/docs).
Был использован движок админки backpack. Сыылку не документацию можно найти [здесь](https://backpackforlaravel.com/)

### Стек технологий
* php7.2
* Laravel5
* nodejs
* webpack(laravel-mix)
* stylus
* jquery
* bootstrap**4**

### Настройка и запуск сервиса
Всё как и у обычного laravel
```bash
composer install --no-dev
npm i

cp .env.example .env
php artisan key:generate
vim .env

npm run prod
php artisan migrate --seed
```

#### Настройка на дев/продакшн сервере
есть 2 варианта:
1. использовать docker. docker-compose файл лежит в корне проекта.
2. настраисать nginx(или apache) + php7.2 + mysql сервер.

#### В файле `.env` писутствуют нестандартные переменные:

* `QIWI_WALLET` -- является счётом-буфером на который будут поступать и с которого будут сниматься деньги и записывается как простой телефон (пример: `QIWI_WALLET="+796987654321"`)  
* `QIWI_WALLET_KEY` -- ключ апи от сервиса qiwi. можно создать [здесь](https://qiwi.com/api). (пример: `QIWI_WALLET_KEY="8cbb786baf50aeceb9c673bb576ac"`)
* `BITCOIN_WALLET` -- является счётом-буфером на который будут поступать и с которого будут сниматься деньги и записывается как биткоин кошелёк. (пример: `BITCOIN_WALLET=21fYWoDDNLXTmkc5gGQza50E66T3Pwyr73`)  
* `BITCOIN_API_HOST` -- ip или хост до сервера bitcoind.  
* `BITCOIN_API_POST` -- порт сервера bitcoind.  
* `BITCOIN_API_URL` -- префикс для http api запроса для bitcoind. (лучше оставить `null`)  
* `BITCOIN_USERNAME` -- логин для bitcoind демона. (смотри -rpcuser у bitcoind). если отсутствует или `BITCOIN_USERNAME=null` попытки логина не будет (а сразу запросы)  
* `BITCOIN_PASSWORD` -- пароль для bitcoind демона. (смотри -rpcpassword у bitcoind)

### Интеграция в готовый laravel сервис

* Сверяем и дополняем зависимости compose и npm.
* Переносим вышеуказанные параметры окружения.
* Переносим файлы конфигрураций (config/exchange.php, config/blade-javascript.php). (просто коприруем в свою `config` папку проекта)
* Переносим контроллеры и прочие файлы проекта. Они находятся в папках: `app/*`, `database/migrations/*`, `database/seeds/*`, `resources/assets/*`, `resources/views/*`
* Переносим роуты и объединяем с имеющимися.
* Настраиваем группы и мидлвэры для групп роутов. (ибо сейчас в админку можно зайти любому)
* Понимаем и интегрируем код провайдеров.