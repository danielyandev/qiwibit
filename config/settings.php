<?php

return [
    'wallet' => [
        'qiwi'    => [
            'surcharge' => 5,
            'rate'      => 573495.84,
            'min'       => 4000,
            'limits'    => [
                'per_month' => 40000,
                'max'       => 15000,
            ],
        ],
        'bitcoin' => [
            'rate'               => 573495.84,
            'min'                => 0.00001,
            'surcharge'          => 5,
            'confirmations'      => 5,
            'fee'                => 0.0001,
            'buffer_wallet'      => env('BITCOIN_WALLET', '~~NO_BITCOIN_BUFFER'),
            'buffer_wallet_name' => env('BITCOIN_WALLET_NAME', '~~NO_BITCOIN_BUFFER'),
            'account'            => env('BITCOIN_ACCOUNT', null),
            'username'           => env('BITCOIN_USERNAME', '~~NO_BITCOIN_USERNAME'),
            'password'           => env('BITCOIN_PASSWORD', '~~NO_BITCOIN_PASSWORD'),
            'api_host'           => env('BITCOIN_API_HOST', 'localhost'),
            'api_port'           => env('BITCOIN_API_POST', 8332),
            'api_url'            => env('BITCOIN_API_URL', null),
        ],
    ],
];