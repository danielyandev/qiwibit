@extends('layout.user')

@section('content')
    @include('client.exchange-form')
    @include('client.components.support')
@endsection