<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('qiwi_wallets') }}"><i class="fa fa-credit-card"></i> <span>Киви кошельки</span></a></li>
<li><a href="{{ backpack_url('bitcoin_wallet') }}"><i class="fa fa-bitcoin"></i> <span>Биткоин переводы</span></a></li>
<li><a href="{{ backpack_url('exchange') }}"><i class="fa fa-list"></i> <span>Обмены</span></a></li>

<li><a href="{{ backpack_url('setting') }}"><i class="fa fa-cog"></i> <span>Настройки</span></a></li>