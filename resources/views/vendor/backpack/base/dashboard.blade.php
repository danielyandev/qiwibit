@extends('backpack::layout')

@push('before_styles')
    <link rel="stylesheet" href="{{ mix('css/admin.css') }}">
@endpush

@push('before_scripts')
    @javascript($js_data)
@endpush

@section('header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::base.dashboard') }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">{{ trans('backpack::base.dashboard') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row" id="dashboard">
        <div class="col-sm-4">
            @include('admin.dashboard.cards.qiwi_stats')
            @include('admin.dashboard.cards.bitcoin_stats')
            @include('admin.dashboard.cards.wallet_state_graph')
        </div>
        <div class="col-sm-4">
            @include('admin.dashboard.cards.trages_state_graph')
            @include('admin.dashboard.cards.trages_state_table')
            @include('admin.dashboard.cards.last_exchanges')
        </div>
        <div class="col-sm-4">
            @include('admin.dashboard.cards.surcharge_edit')
            @include('admin.dashboard.cards.rate_edit')
            @include('admin.dashboard.cards.rate_info')
            @include('admin.dashboard.cards.profit_state_graph')
        </div>
    </div>
@endsection
