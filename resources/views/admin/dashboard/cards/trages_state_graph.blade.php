@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6 class="mb-4">Динамика обмена за 10 дней</h6>

    <canvas class="graph" data-graph-conroller="line_graph" data-graph-source="trade_graph_data"></canvas>
@endcomponent