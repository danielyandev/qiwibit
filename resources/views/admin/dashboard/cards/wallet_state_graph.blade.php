@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6 class="mb-4">Динамика состояния счетов за 10 дней</h6>

    <canvas class="graph" data-graph-conroller="line_graph" data-graph-source="wallet_graph_data"></canvas>
@endcomponent