@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6>Последние {{ $last_ten_exchanges->count() }} успешных обменов</h6>

    <table class="table table-striped">
        <thead>
            <tr>
                <td>#</td>
                <td>Сумма</td>
                <td>Направление</td>
                <td>Выгода</td>
                <td>Создан</td>
            </tr>
        </thead>
        <tbody>
            @foreach($last_ten_exchanges as $exchange)
                <tr>
                    <td>{{ $exchange->id }}</td>
                    <td>{{ $exchange->from->receive_count }}</td>
                    <td>to {{ class_basename($exchange->from->getMorphClass()) }}</td>
                    <td>{{ $exchange->profit }}</td>
                    <td>{{ $exchange->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endcomponent