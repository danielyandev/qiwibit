@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6>Курс с учётом выбранного номинала</h6>

    <p class="lead m-0">
        1 BTC -> {{ config('settings.wallet.bitcoin.rate') }} RUB
    </p>

    <p class="lead">
        {{ config('settings.wallet.qiwi.rate') }} RUB -> 1 BTC
    </p>

@endcomponent