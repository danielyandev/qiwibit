@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6 class="mb-4">Прибыль за последние 10 дней</h6>

    <canvas class="graph" data-graph-conroller="bar_graph" data-graph-source="profit_graph_data"></canvas>
@endcomponent