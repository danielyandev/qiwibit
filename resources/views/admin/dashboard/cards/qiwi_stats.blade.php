@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6>Состояние счетов QIWI на сегодня</h6>
    <span class="mt-2 fa-3x">
        <i class="fa fa-rub"></i> {{ $qiwi_stats['value'] }}
    </span>
    <hr>
    <div class="clearfix">
        <div class="pull-left">
            <span class="d-block">Вчера</span>
            <span class="d-block"><i class="fa fa-rub"></i> {{ $qiwi_stats['yesterday'] }}</span>
        </div>
        <div class="pull-right">
            <span class="d-block">Измерение за неделю</span>
            <span class="d-block"><i class="fa fa-rub"></i> {{ $qiwi_stats['per_week'] }}</span>
        </div>
    </div>
@endcomponent