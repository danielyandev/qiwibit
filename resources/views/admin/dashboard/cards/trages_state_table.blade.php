@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6>Статистика заявок на обмен</h6>

    <div class="nav nav-tabs" id="trades_state" role="tablist">
        <a class="nav-item nav-link active" id="home-tab" data-toggle="tab"
           href="#btc-qiwi" role="tab" aria-controls="btc-qiwi" aria-selected="true">BTC - QIWI</a>
        <a class="nav-item nav-link" id="profile-tab" data-toggle="tab"
           href="#qiwi-btc" role="tab" aria-controls="qiwi-btc" aria-selected="false">QIWI - BTC</a>
    </div>

    <div class="tab-content" id="trades_state_content">
        <div class="tab-pane fade show active" id="btc-qiwi" role="tabpanel" aria-labelledby="btc-qiwi-tab">
            <table class="table table-striped" style="display:block;width:100%;overflow-x:auto">
                <thead>
                    <tr>
                        <td></td>
                        <td>Сегодня</td>
                        <td>Неделя</td>
                        <td>Месяц</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Всего</td>
                        <td>{{ $trages_state_table['btc_qiwi']['all']['today'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['all']['week'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['all']['month'] }}</td>
                    </tr>
                    <tr>
                        <td>Одобрено</td>
                        <td>{{ $trages_state_table['btc_qiwi']['approved']['today'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['approved']['week'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['approved']['month'] }}</td>
                    </tr>
                    <tr>
                        <td>Отклонено</td>
                        <td>{{ $trages_state_table['btc_qiwi']['rejected']['today'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['rejected']['week'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['rejected']['month'] }}</td>
                    </tr>
                    <tr>
                        <td>Изменение</td>
                        <td>{{ $trages_state_table['btc_qiwi']['diff']['today'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['diff']['week'] }}</td>
                        <td>{{ $trages_state_table['btc_qiwi']['diff']['month'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="qiwi-btc" role="tabpanel" aria-labelledby="qiwi-btc-tab">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td></td>
                        <td>Сегодня</td>
                        <td>Неделя</td>
                        <td>Месяц</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Всего</td>
                        <td>{{ $trages_state_table['qiwi_btc']['all']['today'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['all']['week'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['all']['month'] }}</td>
                    </tr>
                    <tr>
                        <td>Одобрено</td>
                        <td>{{ $trages_state_table['qiwi_btc']['approved']['today'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['approved']['week'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['approved']['month'] }}</td>
                    </tr>
                    <tr>
                        <td>Отклонено</td>
                        <td>{{ $trages_state_table['qiwi_btc']['rejected']['today'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['rejected']['week'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['rejected']['month'] }}</td>
                    </tr>
                    <tr>
                        <td>Изменение</td>
                        <td>{{ $trages_state_table['qiwi_btc']['diff']['today'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['diff']['week'] }}</td>
                        <td>{{ $trages_state_table['qiwi_btc']['diff']['month'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endcomponent