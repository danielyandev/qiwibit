@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6>Надбавка (%)</h6>

    <form action="{{ url('/admin/settings_set') }}" method="post" class="clearfix">
        {{ csrf_field() }}
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-rouble"></i></span>
            <input type="text" class="form-control" placeholder="%" name="settings[wallet.qiwi.surcharge]"
                   value="{{ config('settings.wallet.qiwi.surcharge') }}">
        </div>
        <div class="input-group mt-2">
            <span class="input-group-addon"><i class="fa fa-bitcoin"></i></span>
            <input type="text" class="form-control" placeholder="%" name="settings[wallet.bitcoin.surcharge]"
                   value="{{ config('settings.wallet.bitcoin.surcharge') }}">
        </div>
        <input type="submit" class="btn btn-primary pull-right mt-2" value="Применить">
    </form>

@endcomponent