@component('admin.components.admin-card')

    @slot('class', 'mb-4')

    <h6>Изменить курс</h6>
    @foreach($rates ?? [] as $rate)
        <form action="{{ url('/admin/settings_set') }}" method="post">
            <div class="clearfix">
                <div class="pull-left">
                    {{ csrf_field() }}
                    <input type="hidden" name="settings[wallet.qiwi.rate]" value="{{ $rate['rate'] }}">
                    <input type="hidden" name="settings[wallet.bitcoin.rate]" value="{{ $rate['rate'] }}">
                    <p class="lead mb-0">1 BTC = {{ $rate['rate'] }} RUB</p>
                    <small>Курс с сайта <a href="//{{ $rate['link'] }}">{{ $rate['link'] }}</a></small>
                </div>
                <div class="pull-right">
                    <input type="submit" class="btn btn-primary" value="Выбрать">
                </div>
            </div>
            <hr>
        </form>
    @endforeach

@endcomponent