<div>Биткоины</div>
@if($target->direction == 'to')
    <div>Кошелёк пользователя: {{ $target->user_bitcoin_wallet }}</div>
    <div>Будет отправлено: {{ $target->send_count }} BTC</div>
@endif
@if($target->direction == 'from')
    <div>Кошелёк системы: {{ $target->receive_bitcoin_wallet }} BTC</div>
    <div>Будет получено: {{ $target->receive_count }} BTC</div>
@endif
