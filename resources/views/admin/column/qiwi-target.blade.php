<div>Киви</div>
@if($target->direction == 'to')
    <div>Кошелёк пользователя: {{ $target->user_qiwi_wallet }}</div>
    <div>Будет отправлено: {{ $target->send_count }} РУБ</div>
@endif
@if($target->direction == 'from')
    <div>Кошелёк системы: {{ $target->receive_qiwi_wallet }}</div>
    <div>Будет получено: {{ $target->receive_count }} РУБ</div>
@endif
