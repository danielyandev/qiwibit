<nav class="navbar sticky-top navbar-dark bg-dark" id="top-navbar-section">

    <a class="navbar-brand" href="{{ route('admin.dashboard') }}">{{ config('app.name') }}</a>

    <ul class="nav col justify-content-start">
        <li class="nav-item">
            <a class="nav-link navbar-text" href="#">Link 1</a>
        </li>
        <li class="nav-item">
            <a class="nav-link navbar-text" href="#">Link 2</a>
        </li>
    </ul>
    <ul class="nav justify-content-end">
        <li class="nav-item">
            <a class="nav-link navbar-text" href="#"><i class="fa fa-bell"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link navbar-text" href="#"><i class="fa fa-user"></i></a>
        </li>
    </ul>
</nav>