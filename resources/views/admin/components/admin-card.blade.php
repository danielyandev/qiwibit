<div class="card admin-card {!! $card_class ?? $class ?? '' !!}">
    @if($header ?? false)
        <div class="card-header {!! $header_class ?? '' !!}">
            {!! $header !!}
        </div>
    @endif
    @if($pure ?? false)
        {!! $slot !!}
    @else
        <div class="card-body">
            {!! $slot !!}
        </div>
    @endif
</div>