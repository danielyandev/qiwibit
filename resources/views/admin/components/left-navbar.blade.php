<div id="left-navbar">
    <nav class="nav flex-column align-items-stretch h-100">
        <span class="nav-link nav-link-avatar"><img src="//placehold.it/50x50" alt="avatar" class="img-fluid"></span>
        <a class="nav-link active" href="#"><i class="fa fa-bell"></i></a>
        <a class="nav-link" href="#"><i class="fa fa-rub"></i></a>
        <a class="nav-link" href="#"><i class="fa fa-bitcoin"></i></a>
        <a class="nav-link" href="#"><i class="fa fa-shield"></i></a>
    </nav>
</div>