@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Биткоин переводы
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <a href="{{ backpack_url('bitcoin_wallet/send') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Отправить</a>
        </div>
        <div class="box-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>Имя кошелька (если есть)</td>
                        <td>Адрес кошелька</td>
                        <td>Направление перевода</td>
                        <td>Сумма</td>
                        <td>Fee</td>
                        <td>Подтверждения</td>
                        <td>TXID</td>
                        <td>Время</td>
                        <td>Отменён</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $row)
                        <tr>
                            <td>
                                {{ $row['account'] ?? '' }}
                            </td>
                            <td>
                                {{ $row['address'] }}
                            </td>
                            <td>
                                {{ $row['category'] }}
                            </td>
                            <td>
                                {{ $row['amount'] }}
                            </td>
                            <td>
                                {{ $row['fee'] ?? '-' }}
                            </td>
                            <td>
                                {{ $row['confirmations'] }}
                            </td>
                            <td>
                                {{ $row['txid'] }}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::createFromTimestamp($row['time']) }}
                            </td>
                            <td>
                                {{ $row['abandoned'] ?? false ? 'Да' : 'Нет' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection


@section('after_styles')
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
    <script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/show.js') }}"></script>
@endsection
