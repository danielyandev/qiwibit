@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">{{ $crud->entity_name_plural }}</span>
            <small>{{ ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name }}.</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')

    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}">
            <i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
            <span>{{ $crud->entity_name_plural }}</span>
        </a><br><br>
    @endif

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                Просмотр истории кошелька (за последние 59 дней)
            </h3>
        </div>
        <div class="box-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>Из</td>
                        <td>В</td>
                        <td>Статус</td>
                        <td>Напрпвление перевода</td>
                        <td>Комментарий</td>
                        <td>Сумма</td>
                        <td>Дата</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($history as $row)
                        <tr>
                            <td>
                                {{ $row['account'] }}
                            </td>
                            <td>
                                {{ $row['personId'] }}
                            </td>
                            <td>
                                {{ $row['statusText'] }}
                            </td>
                            <td>
                                {{ $row['type'] }}
                            </td>
                            <td>
                                {{ str_limit($row['comment'], 150) }}
                            </td>
                            <td>
                                {{ $row['sum']['amount'] ?? '-' }}
                            </td>
                            <td>
                                {{ $row['date'] ? \Carbon\Carbon::parse($row['date']) : '-' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection


@section('after_styles')
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
    <script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/show.js') }}"></script>
@endsection
