@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            Отправка биткоинов
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <a href="{{ backpack_url('/bitcoin_wallet') }}">
                <i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
                <span>Назад</span>
            </a><br><br>

            <form action="{{ URL::current() }}" method="post">
                {{ csrf_field() }}
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Отправка средств из кошелька {{ config('settings.wallet.bitcoin.buffer_wallet') }}
                        </h3>
                    </div>
                    <div class="box-body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="receiver">Получатель</label>
                            <input type="text" class="form-control" name="receiver" id="receiver">
                        </div>

                        <div class="form-group">
                            <label for="sum">Сумма</label>
                            <input type="text" class="form-control" name="sum" id="sum">
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-arrow-circle-right" role="presentation" aria-hidden="true"></span> &nbsp;
                            <span data-value="save_and_back">Отправить</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('after_styles')
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
    <script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('vendor/backpack/crud/js/show.js') }}"></script>
@endsection
