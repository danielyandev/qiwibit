<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @stack('head')
        <title>@yield('title', config('app.name'))</title>
    </head>
    <body>
        @yield('content')
        @stack('scripts')
    </body>
</html>