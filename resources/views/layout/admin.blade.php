@extends('layout.master')

@push('head')
    <link rel="stylesheet" href="{{ mix('/css/admin.css') }}">
@endpush

@push('scripts')
    <script src="{{ mix('/js/admin.js') }}" lang="text/javascript"></script>
    @javascript($js_data ?? [])
@endpush

@section('content')
    @include('admin.components.top-nav')
    @include('admin.components.left-navbar')
    <div class="container-fluid admin-wrapper">
        @yield('admin-panel-content')
    </div>
@endsection