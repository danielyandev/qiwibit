@extends('layout.master')

@push('head')
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
@endpush

@push('scripts')
    <script src="{{ mix('/js/app.js') }}" lang="text/javascript"></script>
    @javascript($js_data ?? [])
@endpush