@extends('layout.user')

@section('content')
    <div id="exchange-section">
        <div class="container">
            <div class="row flex-column justify-content-center vh-100">
                <div class="col-0">
                    <div class="exchange-card">
                        <h1>Как оплатить</h1>
                        <p>
                            <b>Переведите точную сумму</b>: {{ $exchange->from->receive_count }} BTC
                        </p>
                        <p>
                            <b>На кошелек</b>: {{ $exchange->from->creteReceiveWallet() }}
                        </p>
                        <p>
                            <b>Идентификатор обмена:</b> id{{ $exchange->id }}
                        </p>
                        <p>
                            <b>Время создания:</b> {{ $exchange->created_at }}
                        </p>
                        <p>
                            <b>Статус:</b> {{ $exchange->disply_status }}
                        </p>
                        <p class="text-warning font-weight-bold">
                            Пожалуйста, будьте внимательны!<br>
                            Все поля должны быть заполнены в точном соответствии с инструкцией. В противном случае,
                            платеж может не пройти.
                        </p>
                        <p class="font-weight-light">
                            Данная операция занимает от 5 до 90 минут. Платеж считается полученным при накоплении 2х
                            подтверждений со стороны Blockchain.
                        </p>
                        <div class="row no-gutters mt-3 mb-4">
                            <div class="col">
                                <button type="submit" onclick="window.location.reload()"
                                        class="btn btn-lg btn-block btn-secondary">Обновить статус
                                </button>
                            </div>
                            @if($exchange->status == \App\Models\Exchange::CREATED)
                                <div class="col-0">
                                    <button type="submit"
                                            onclick="window.location = '{{ route('exchange-discard', ['from' => $exchange->from, 'id' => $exchange->id, 'email' => $exchange->email]) }}'"
                                            class="btn btn-lg btn-block btn-danger ml-2">Отменить обмен
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('client.components.support')
@endsection