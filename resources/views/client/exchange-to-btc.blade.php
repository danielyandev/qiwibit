@component('client.components.client-exchange-form')

    @slot('form_action', route('change-action', ['from' => 'qiwi', 'to' => 'bitcoin']))

    <h2>Автоматический обмен QIWI &RightArrow; BitCoin</h2>
    <small>{{ config('settings.wallet.qiwi.surcharge') > 0 ? 'Комиссия' : 'Бонус' }}:
        {{ abs(config('settings.wallet.qiwi.surcharge')) }}%</small>

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">{{ $error }}</div>
        @endforeach
    @endif

    <div class="row">

        <div class="col-md-6">

            <label for="receive_count">Отдаёте</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-rub"></i>
                    </span>
                </div>
                <input type="text" class="form-control" id="receive_count" name="receive_count" value="{{ old('receive_count') }}"
                       placeholder="рублей qiwi" required>
            </div>

            <label for="user_qiwi_wallet">С кошелька</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-rub"></i>
                    </span>
                </div>
                <input type="text" class="form-control" id="user_qiwi_wallet" name="user_qiwi_wallet"
                       value="{{ old('user_qiwi_wallet') }}"
                       placeholder="+7 ..." required>
            </div>

        </div>


        <div class="col-md-6">

            <label for="bitcoin_prewiev">Получаете</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-bitcoin"></i>
                    </span>
                </div>
                <input type="text" class="form-control" id="bitcoin_preview" readonly value="0 btc">
            </div>

            <label for="user_bitcoin_wallet">На кошелёк</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-bitcoin"></i>
                    </span>
                </div>
                <input type="text" class="form-control" id="user_bitcoin_wallet" name="user_bitcoin_wallet"
                       value="{{ old('user_bitcoin_wallet') }}" placeholder="############" required>
            </div>

            <label for="email">Почта</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-envelope"></i>
                    </span>
                </div>
                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                       placeholder="example@yandex.ru" required>
            </div>

        </div>


        <div class="col-md-8 offset-md-2">
            <input type="submit" value="ОБМЕНЯТЬ" class="btn btn-lg btn-block btn-secondary mt-3 mb-4">
        </div>

    </div>
@endcomponent