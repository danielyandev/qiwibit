<div class="exchange-form">
    <form action="{!! $form_action !!}" method="post">
        {!! csrf_field() !!}
        {!! $slot !!}
    </form>
</div>
