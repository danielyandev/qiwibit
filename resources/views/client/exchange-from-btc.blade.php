@component('client.components.client-exchange-form')

    @slot('form_action', route('change-action', ['from' => 'bitcoin', 'to' => 'qiwi']))

    <h2>Автоматический обмен BitCoin &RightArrow; QIWI</h2>
    <small>{{ config('settings.wallet.bitcoin.surcharge') > 0 ? 'Комиссия' : 'Бонус' }}:
        {{ abs(config('settings.wallet.bitcoin.surcharge')) }}%</small>

    <div class="row">

        <div class="col-md-6">

            <label for="receive_count">Отдаёте</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-bitcoin"></i>
                    </span>
                </div>
                <input type="text" class="form-control" id="receive_count" name="receive_count"
                       placeholder="btc" value="{{ old('receive_count') }}" required>
            </div>

        </div>


        <div class="col-md-6">

            <label for="qiwi_prewiev">Получаете</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-rub"></i>
                    </span>
                </div>
                <input type="text" class="form-control" id="qiwi_preview" readonly value="0 rub">
            </div>

            <label for="user_qiwi_wallet">На кошелёк</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-rub"></i>
                    </span>
                </div>
                <input type="text" class="form-control" id="user_qiwi_wallet" name="user_qiwi_wallet"
                       placeholder="+7 ..." value="{{ old('user_qiwi_wallet') }}" required>
            </div>

            <label for="email">Почта</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-envelope"></i>
                    </span>
                </div>
                <input type="email" class="form-control" id="email" name="email"
                       placeholder="example@yandex.ru" required>
            </div>

        </div>


        <div class="col-md-8 offset-md-2">
            <input type="submit" value="ОБМЕНЯТЬ" class="btn btn-lg btn-block btn-secondary mt-3 mb-4">
        </div>

    </div>
@endcomponent