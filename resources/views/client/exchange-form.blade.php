<div id="exchange-section">
    <div class="container">

        <div class="row flex-column justify-content-center vh-100">
            <div class="col-0">
                <div class="exchange-card">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a
                                    class="nav-item nav-link active"
                                    id="to-btc-tab"
                                    data-toggle="tab"
                                    href="#to-btc"
                                    role="tab"
                                    aria-controls="to-btc"
                                    aria-selected="true">
                                QIWI - BTC
                            </a>
                            <a
                                    class="nav-item nav-link"
                                    id="from-btc-tab"
                                    data-toggle="tab"
                                    href="#from-btc"
                                    role="tab"
                                    aria-controls="from-btc"
                                    aria-selected="false">
                                BTC - QIWI
                            </a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div
                                class="tab-pane fade show active"
                                id="to-btc"
                                role="tabpanel"
                                aria-labelledby="to-btc-tab">
                            @include('client.exchange-to-btc')
                        </div>
                        <div
                                class="tab-pane fade"
                                id="from-btc"
                                role="tabpanel"
                                aria-labelledby="from-btc-tab">
                            @include('client.exchange-from-btc')
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-0 text-center">
                <span>
                    Лимит: {{ $free_qiwi }} руб; {{ $free_btc }} btc
                </span>
            </div>

        </div>

    </div>
</div>