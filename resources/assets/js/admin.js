import './init';

import 'bootstrap/dist/js/bootstrap.bundle';
import * as graphs from "./admin/graphs/graphs";
import $ from "jquery";

/**
 * Главный js файл для админ панели.
 */

$(document).ready(() => {
    graphs.apply();
});
