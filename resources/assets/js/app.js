import './init'
import 'bootstrap/dist/js/bootstrap.bundle'

import $ from 'jquery'

/**
 * Главный js файл для стороны юзера.
 */

$(document).ready(() => {
    $('#to-btc, #from-btc').each((i, ele) => {
        const e = $(ele)
        const from = e.attr('id') === 'to-btc'
        const from_name = from ? 'qiwi' : 'bitcoin'
        const from_field = e.find(`#receive_count`)
        const preview_field = e.find(`#${!from ? 'qiwi' : 'bitcoin'}_preview`)
        const update = c => {
            const formatted = c.target.value.replace(/[\.,]/g, '.')
            const val = parseFloat(formatted)
            if (val) {
                const price = val * (from ? 1 / window.data.rate_qiwi : window.data.rate_btc) * (1 - window.data[`surcharge_${from_name}`] / 100)
                preview_field.val(price + ' ' + (from ? 'btc' : 'rub'))
                if (c.target.value != formatted)
                    c.target.value = formatted
            } else preview_field.val('0 ' + (from ? 'btc' : 'rub'))
        }
        from_field.change(update)
        from_field.keyup(update)
    })
})