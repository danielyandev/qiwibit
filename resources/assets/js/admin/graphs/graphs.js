import $ from "jquery";
import controllers from "./controllers";
import _ from "lodash";

/**
 * Обходим документ, настраиваем и наполняем графики.
 * @param target
 */
export function apply(target = document) {
    let root = $(target);
    let graphs = root.find('[data-graph-conroller]');

    graphs.each((i, ele) => {
        let jqele = $(ele);
        let controller = jqele.attr('data-graph-conroller');
        let data_source = jqele.attr('data-graph-source');
        if (controller in controllers) {
            jqele.height(jqele.height()); // надо конкретно в аттрибутах задать высоту иначе графики поплывут
            controllers[controller](ele, _.at(window, 'data.' + data_source) || null);
        } else console.error(`Controller "${controller}" not found for`, ele);

    });
}