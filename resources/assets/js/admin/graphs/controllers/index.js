import line_graph from "./line_graph";
import bar_graph from "./bar_graph";

/**
 * Контроллеры здесь определяют какой тип
 * диограммы будет отображён для имеющихся данных.
 */
export default {
    line_graph,
    bar_graph
}