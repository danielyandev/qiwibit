import Chart from "chart.js";
import _ from "lodash";

export default (element, data) => {
    new Chart(element, {
        type: 'line',
        options: {
            legend: {
                position: 'bottom'
            },
        },
        data: _.isArray(data) ? data[0] : data
    });
}