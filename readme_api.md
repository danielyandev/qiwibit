## API для обменника

Используем http протокол для создания запросов.

Для каждого запроса желательно добавлять хеддер `Accept` дабы предотвратить отображение дефолтного экрана ошибок и складывать их сообщения в json.
`Accept: application/json`

Для всех запросов, предполагающих получение данных от пользователя необходима авторизация.
Авторизация происходит по ключу.

В каждый запрос необходимо добавлять хеддер авторизации:
`Authorization: Bearer {api_token}`

### Состояния счетов

`GET /api/wallets`

Возвращает состояние счетов
```json
{
    "bitcoin": 0.0006237,
    "qiwi": 1004.5
}
```

### Статистика обменов

`GET /api/statistics`

Возвращает статистику обменов
```json
{
    "today": {
        "all": 5,
        "approved": 3,
        "error": 2
    },
    "week": {
        "all": 80,
        "approved": 75,
        "error": 5
    },
    "year": {
        "all": 653,
        "approved": 640,
        "error": 13
    }
}
```

### Выставление настроек

`POST /api/set_settings`

В тело запроса надо вставить `settings` массив с ключами и значениями конфигураций.

| Ключ | Описание |
| --- | --- |
|wallet.qiwi.surcharge | Надбавка, когда мы получаем киви рубли|
|wallet.qiwi.min | Минимальное значение киви|
|wallet.qiwi.limits.per_month | Стандартное значение максимального значен| переводов за месяц",
|wallet.qiwi.limits.max | Стандартное значение максимального значения кикошелька|
|wallet.qiwi.rate | Курс rub->btc|
|wallet.bitcoin.surcharge | Надбавка, когда мы получаем биткоины|
|wallet.bitcoin.min | Минимальное значение биткоина|
|wallet.bitcoin.fee | Bitcoin fee|
|wallet.bitcoin.name | Bitcoin wallet name|
|wallet.bitcoin.rate | Курс btc->rub|