<?php

namespace App\Providers;

use App;
use App\Bitcoin;
use Config;
use Exception;
use Illuminate\Support\ServiceProvider;
use Schema;
use Setting;

class AppServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        try {
            if (App::runningInConsole() && count(Schema::getColumnListing('settings'))) {
                // get all settings from the database
                $settings = Setting::all();

                // bind all settings to the Laravel config, so you can call them like
                // Config::get('settings.contact_email')
                foreach ($settings as $key => $setting) {
                    Config::set('settings.'.$setting->key, $setting->value);
                }
            }
        } catch (Exception $e) {
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        /*
         * Регистрируем экземпляр биткоин апи
         */
        $this->app->singleton('bitcoin-api', function () {
            $api = new Bitcoin(
                config('settings.wallet.bitcoin.username'),
                config('settings.wallet.bitcoin.password'),
                config('settings.wallet.bitcoin.api_host'),
                config('settings.wallet.bitcoin.api_port'),
                config('settings.wallet.bitcoin.api_url')
            );

            if (config('settings.wallet.bitcoin.account'))
                $api->setaccount(config('settings.wallet.bitcoin.buffer_wallet'), config('settings.wallet.bitcoin.account'));

            return $api;
        });
    }
}
