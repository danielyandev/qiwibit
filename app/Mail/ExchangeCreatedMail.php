<?php

namespace App\Mail;

use App\Models\Exchange;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ExchangeCreatedMail extends Mailable {
    use Queueable, SerializesModels;

    protected $exchange;

    public function __construct(Exchange $exchange) {
        $this->exchange = $exchange;
    }

    public function build() {
        return $this
            ->view('mail.exchange-created')
            ->with(['exchange' => $this->exchange]);
    }
}
