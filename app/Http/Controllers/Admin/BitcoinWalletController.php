<?php

namespace App\Http\Controllers\Admin;

use App\BitcoinApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BitcoinWalletController extends Controller {

    public function history() {
        $transactions = collect(BitcoinApi::listtransactions()->result())->reverse()->take(200);

        return view('admin.pages.bitcoin-history', compact('transactions'));
    }

    public function showSendForm() {
        return view('admin.pages.bitcoin-send');
    }

    public function sendBitCoins(Request $request) {
        $v = $request->validate([
            'receiver' => 'required|regex:/[13][a-km-zA-HJ-NP-Z1-9]{25,34}/',
            'sum'      => 'required|numeric',
        ]);

        $receiver = $v['receiver'];
        $sum = floatval($v['sum']);

        $exchange = BitcoinApi::sendfrom(
            config('settings.wallet.bitcoin.buffer_wallet_name'),
            $receiver,
            $this->correctBticoinAmount($sum)
        );

        return back()->with('message', $exchange);

    }

    public function correctBticoinAmount($amount) {
        return round(floatval($amount) * 100000000) / 100000000;
    }
}
