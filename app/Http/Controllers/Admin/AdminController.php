<?php

namespace App\Http\Controllers\Admin;

use App\BitcoinApi;
use App\Models\Exchange;
use App\Models\ExchangeTargets\BitcoinTarget;
use App\Models\ExchangeTargets\QiwiTarget;
use App\Models\QiwiWallet;
use App\Models\WalletState;
use Backpack\Base\app\Http\Controllers\AdminController as DashboardController;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

class AdminController extends DashboardController {

    /**
     * Отдаём данные для дашборда
     * @return \Illuminate\Http\Response
     */
    public function dashboard() {
        $now = QiwiWallet::valid()->get()->sum('available_balance');

        $yesterday = WalletState::where('wallet', 'qiwi')
            ->where('created_at', '<', Carbon::now()->subDay())
            ->latest()
            ->first();

        $week = WalletState::where('wallet', 'qiwi')
            ->where('created_at', '<', Carbon::now()->subDay(7))
            ->latest()
            ->first();

        $qiwi_stats = [
            'value'     => $now ?: 0,
            'yesterday' => $yesterday ? $yesterday->value : 0,
            'per_week'  => ($now ?: 0) - ($week ? $week->value : 0),
        ];

        $now = BitcoinApi::getbalance()->response['result'];

        $yesterday = WalletState::where('wallet', 'bitcoin')
            ->where('created_at', '<', Carbon::now()->subDay())
            ->latest()
            ->first();

        $week = WalletState::where('wallet', 'bitcoin')
            ->where('created_at', '<', Carbon::now()->subDay(7))
            ->latest()
            ->first();

        $bitcoin_stats = [
            'value'     => $now ?: 0,
            'yesterday' => $yesterday ? $yesterday->value : 0,
            'per_week'  => ($now ?: 0) - ($week ? $week->value : 0),
        ];

        $trages_state_table = [
            'btc_qiwi' => [
                'all'      => [
                    'today' => Exchange::fromBitcoin()
                        ->where('created_at', '>', Carbon::now()->subDay())->count(),
                    'week'  => Exchange::fromBitcoin()
                        ->where('created_at', '>', Carbon::now()->subWeek())->count(),
                    'month' => Exchange::fromBitcoin()
                        ->where('created_at', '>', Carbon::now()->subMonth())->count(),
                ],
                'approved' => [
                    'today' => Exchange::fromBitcoin()->accepted()
                        ->where('created_at', '>', Carbon::now()->subDay())->count(),
                    'week'  => Exchange::fromBitcoin()->accepted()
                        ->where('created_at', '>', Carbon::now()->subWeek())->count(),
                    'month' => Exchange::fromBitcoin()->accepted()
                        ->where('created_at', '>', Carbon::now()->subMonth())->count(),
                ],
                'rejected' => [
                    'today' => Exchange::fromBitcoin()->errored()
                        ->where('created_at', '>', Carbon::now()->subDay())->count(),
                    'week'  => Exchange::fromBitcoin()->errored()
                        ->where('created_at', '>', Carbon::now()->subWeek())->count(),
                    'month' => Exchange::fromBitcoin()->errored()
                        ->where('created_at', '>', Carbon::now()->subMonth())->count(),
                ],
                'diff'     => [
                    'today' => $this->calcDiff(
                        Exchange::fromBitcoin()
                            ->where('created_at', '>', Carbon::now()->subDay())
                            ->count(),
                        Exchange::fromBitcoin()
                            ->where('created_at', '<', Carbon::now()->subDay())
                            ->where('created_at', '>', Carbon::now()->subDay(2))
                            ->count()
                    ),
                    'week'  => $this->calcDiff(
                        Exchange::fromBitcoin()
                            ->where('created_at', '>', Carbon::now()->subWeek())
                            ->count(),
                        Exchange::fromBitcoin()
                            ->where('created_at', '<', Carbon::now()->subWeek())
                            ->where('created_at', '>', Carbon::now()->subWeek(2))
                            ->count()
                    ),
                    'month' => $this->calcDiff(
                        Exchange::fromBitcoin()
                            ->where('created_at', '>', Carbon::now()->subMonth())
                            ->count(),
                        Exchange::fromBitcoin()
                            ->where('created_at', '<', Carbon::now()->subMonth())
                            ->where('created_at', '>', Carbon::now()->subMonth(2))
                            ->count()
                    ),
                ],
            ],
            'qiwi_btc' => [
                'all'      => [
                    'today' => Exchange::toBitcoin()
                        ->where('created_at', '>', Carbon::now()->subDay())->count(),
                    'week'  => Exchange::toBitcoin()
                        ->where('created_at', '>', Carbon::now()->subWeek())->count(),
                    'month' => Exchange::toBitcoin()
                        ->where('created_at', '>', Carbon::now()->subMonth())->count(),
                ],
                'approved' => [
                    'today' => Exchange::toBitcoin()->accepted()
                        ->where('created_at', '>', Carbon::now()->subDay())->count(),
                    'week'  => Exchange::toBitcoin()->accepted()
                        ->where('created_at', '>', Carbon::now()->subWeek())->count(),
                    'month' => Exchange::toBitcoin()->accepted()
                        ->where('created_at', '>', Carbon::now()->subMonth())->count(),
                ],
                'rejected' => [
                    'today' => Exchange::toBitcoin()->errored()
                        ->where('created_at', '>', Carbon::now()->subDay())->count(),
                    'week'  => Exchange::toBitcoin()->errored()
                        ->where('created_at', '>', Carbon::now()->subWeek())->count(),
                    'month' => Exchange::toBitcoin()->errored()
                        ->where('created_at', '>', Carbon::now()->subMonth())->count(),
                ],
                'diff'     => [
                    'today' => $this->calcDiff(
                        Exchange::toBitcoin()
                            ->where('created_at', '>', Carbon::now()->subDay())
                            ->count(),
                        Exchange::toBitcoin()
                            ->where('created_at', '<', Carbon::now()->subDay())
                            ->where('created_at', '>', Carbon::now()->subDay(2))
                            ->count()
                    ),
                    'week'  => $this->calcDiff(
                        Exchange::toBitcoin()
                            ->where('created_at', '>', Carbon::now()->subWeek())
                            ->count(),
                        Exchange::toBitcoin()
                            ->where('created_at', '<', Carbon::now()->subWeek())
                            ->where('created_at', '>', Carbon::now()->subWeek(2))
                            ->count()
                    ),
                    'month' => $this->calcDiff(
                        Exchange::toBitcoin()
                            ->where('created_at', '>', Carbon::now()->subMonth())
                            ->count(),
                        Exchange::toBitcoin()
                            ->where('created_at', '<', Carbon::now()->subMonth())
                            ->where('created_at', '>', Carbon::now()->subMonth(2))
                            ->count()
                    ),
                ],
            ],
        ];

        $rates = [
            (function () {
                return [
                    'rate' => json_decode((new Client())
                        ->send(new Request('GET', 'https://blockchain.info/ru/ticker'))
                        ->getBody())
                        ->RUB->buy,
                    'link' => 'blockchain.info',
                ];
            })(),
            (function () {
                return [
                    'rate' => json_decode((new Client())
                        ->send(new Request('GET', 'https://wex.nz/api/3/ticker/btc_rur'))
                        ->getBody())
                        ->btc_rur->high,
                    'link' => 'wex.nz',
                ];
            })(),
        ];

        $last_ten_days = collect(range(10, 0))->map(function ($day) {
            return Carbon::now()->subDay($day);
        });

        $last_net_days_disply = $last_ten_days->map(function (Carbon $date) {
            return $date->format('m.d');
        });

        $last_trades = Exchange::addSelect('created_at', 'profit')->with('from', 'to')
            ->addSelect(DB::raw('DAY(created_at) as day_at'))
            ->where('status', Exchange::DONE)
            ->where('created_at', '>', $last_ten_days->first())
            ->where('created_at', '<', $last_ten_days->last())
            ->orderBy('created_at', 'desc')
            ->get();

        $last_ten_exchanges = Exchange::latest()->where('status', Exchange::DONE)->limit(10)->get();

        $trade_graph_data_qiwi = $last_trades->where('from_type', QiwiTarget::class)->groupBy('day_at')->values();

        $trade_graph_data = [
            'labels'   => $trade_graph_data_qiwi->flatten()->map(function ($time) {
                return $time->created_at->format('m.d');
            }),
            'datasets' => [
                [
                    'label'           => 'QIWI - BTC',
                    'backgroundColor' => 'rgba(107, 138, 189, 0.25)',
                    'data'            => $trade_graph_data_qiwi->map(function ($trades) {
                        return $trades->count();
                    }),
                ],
                [
                    'label'           => 'BTC - QIWI',
                    'backgroundColor' => 'rgba(127, 138, 200, 0.25)',
                    'data'            => $last_trades
                        ->where('from_type', BitcoinTarget::class)
                        ->groupBy('day_at')
                        ->values()
                        ->map(function ($trades) {
                            return $trades->count();
                        }),
                ],
            ],
        ];

        $profit_graph_data_btc = $last_trades->where('from_type', BitcoinTarget::class)->groupBy('day_at')->values();
        $profit_graph_data_qiwi = $last_trades->where('from_type', QiwiTarget::class)->groupBy('day_at')->values();

        $profit_graph_data = [
            'labels'   => $profit_graph_data_btc->flatten()->map(function ($time) {
                return $time->created_at->format('m.d');
            }),
            'datasets' => [
                [
                    'label'           => 'BTC',
                    'backgroundColor' => 'rgba(107, 138, 189, 0.25)',
                    'data'            => $profit_graph_data_btc
                        ->map(function ($trades) {
                            return $trades->sum('profit');
                        }),
                ],
                [
                    'label'           => 'QIWI',
                    'backgroundColor' => 'rgba(127, 138, 200, 0.25)',
                    'data'            => $profit_graph_data_qiwi
                        ->map(function (Collection $trades) {
                            return $trades->sum('profit');
                        }),
                ],
            ],
        ];

        $wallet_graph_data_days = WalletState::where('wallet','!=',null)
            ->whereDate('created_at', '>', $last_ten_days->first())
            ->whereDate('created_at', '<', $last_ten_days->last())
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get()
            ->pluck('created_at')
            ->map->format('m.d')
            ->reverse()
            ->values();

        $wallet_graph_data = [
            'labels'   => $wallet_graph_data_days,
            'datasets' => [
                [
                    'label'           => 'QIWI',
                    'backgroundColor' => 'rgba(107, 138, 189, 0.25)',
                    'data'            => WalletState::where('wallet', 'qiwi')
                        ->where('created_at', '>', $last_ten_days->first())
                        ->where('created_at', '<', $last_ten_days->last())
                        ->orderBy('created_at')
                        ->pluck('value'),
                ],
                [
                    'label'           => 'BTC',
                    'backgroundColor' => 'rgba(127, 138, 200, 0.25)',
                    'data'            => WalletState::where('wallet', 'bitcoin')
                        ->where('created_at', '>', $last_ten_days->first())
                        ->where('created_at', '<', $last_ten_days->last())
                        ->orderBy('created_at')
                        ->pluck('value'),
                ],
            ],
        ];

        $this->data['js_data'] = compact('trade_graph_data', 'profit_graph_data', 'wallet_graph_data');
        $this->data = array_merge($this->data, compact('now_val', 'rates', 'js_data', 'qiwi_stats', 'bitcoin_stats', 'trages_state_table', 'last_ten_exchanges'));

        return parent::dashboard();
    }

    private function calcDiff($now, $before) {
        return $before - $now;
    }

    /**
     * Устанавливаем конфиги
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function settingsSet() {
        $settings = Input::get('settings');
        foreach ($settings as $key => $value) {
            if (is_float($value)) $value = floatval($value);
            DB::table('settings')->where(compact('key'))->update(compact('value'));
        }
        return $this->redirect();
    }
}
