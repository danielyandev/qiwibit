<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\QiwiWalletRequest as StoreRequest;
use App\Http\Requests\QiwiWalletRequest as UpdateRequest;
use App\Models\QiwiWallet;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

/**
 * Класс админ панели. Подробности смотри в документации админки.
 *
 * Class QiwiWalletCrudController
 * @package App\Http\Controllers\Admin
 */
class QiwiWalletCrudController extends CrudController {

    public function setup() {
        $this->crud->setModel('App\Models\QiwiWallet');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/qiwi_wallets');
        $this->crud->setEntityNameStrings('Кошелёк', 'Кошельки');


        $this->crud->addColumn([
            'name'  => 'login',
            'label' => "Логин",
        ]);

        $this->crud->addColumn([
            'name'  => 'password',
            'label' => "Пароль",
            'type'  => 'view',
            'view'  => 'admin.column.password',
        ]);

        $this->crud->addColumn([
            'name'  => 'valid',
            'label' => "Рабочий",
            'type'  => 'check',
        ]);

        $this->crud->addColumn([
            'name'  => 'balance',
            'label' => "Баланс",
        ]);

        $this->crud->addColumn([
            'name'  => 'available_balance',
            'label' => "Доступно для перевода",
        ]);

        $this->crud->addColumn([
            'name'  => 'limit_max',
            'label' => "Максимальный баланс",
        ]);

        $this->crud->addColumn([
            'name'  => 'limit_month',
            'label' => "Лимит за месяц",
        ]);


        $this->crud->addField([
            'name'  => 'login',
            'label' => "Логин",
        ]);

        $this->crud->addField([
            'name'  => 'password',
            'label' => "Пароль",
        ]);

        $this->crud->addField([
            'name'    => 'limit_month',
            'label'   => "Лимит за месяц (-1 для: " . config('settings.wallet.qiwi.limits.per_month') . ")",
            'default' => -1,
        ]);

        $this->crud->addField([
            'name'    => 'limit_max',
            'label'   => "Максимальный баланс (-1 для: " . config('settings.wallet.qiwi.limits.max') . ")",
            'default' => -1,
        ]);

        $this->crud->addButtonFromModelFunction('line', 'refresh_link', 'getRefreshButton');
        $this->crud->addButtonFromModelFunction('line', 'history_link', 'getHistoryButton');
        $this->crud->addButtonFromModelFunction('line', 'send_link', 'getSendButton');
    }

    public function store(StoreRequest $request) {
        $redirect_location = parent::storeCrud($request);
        return $redirect_location;
    }

    public function update(UpdateRequest $request) {
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }

    public function showHistory(QiwiWallet $wallet) {

        $history = $wallet->getManager()->history(now()->subDay(59), now())['data'];

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        $this->data['history'] = $history;

        return view('admin.pages.wallet-history')->with($this->data);
    }

    public function refreshBalance(QiwiWallet $wallet) {
        $wallet->updateBalance();
        return back();
    }

    public function showSendForm(QiwiWallet $wallet) {
        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);
        $this->data['wallet'] = $wallet;

        return view('admin.pages.wallet-send')->with($this->data);
    }

    public function send(Request $request, QiwiWallet $wallet) {
        $v = $request->validate([
            'receiver' => 'required|regex:/\+?\d{5,15}/',
            'sum'      => 'required|regex:/\d+(?:\.\d+)?/',
            'comment'  => 'nullable|max:250',
        ]);

        $wallet->getManager()->pay(now()->timestamp, floatval($v['sum']), $v['receiver'], $v['comment'] ?: '');

        return back();
    }
}
