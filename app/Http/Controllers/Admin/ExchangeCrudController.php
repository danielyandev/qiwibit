<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ExchangeRequest as StoreRequest;
use App\Http\Requests\ExchangeRequest as UpdateRequest;
use App\Models\Exchange;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Класс админ панели. Подробности смотри в документации админки.
 *
 * Class ExchangeCrudController
 * @package App\Http\Controllers\Admin
 */
class ExchangeCrudController extends CrudController {

    public function setup() {

        $this->crud->setModel('App\Models\Exchange');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/exchange');
        $this->crud->setEntityNameStrings('Обмен', 'Обмены');
        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->enableExportButtons();
        $this->crud->with('from', 'to');

//        $this->crud->setFromDb();
//        $this->crud->addColumn('created_at');
//        $this->crud->removeColumn('extra');

        $this->crud->addColumn([
            'name'  => 'id',
            'label' => '#',
        ]);

        $this->crud->addColumn([
            'name'  => 'email',
            'label' => 'Почта покупателя',
        ]);

        $this->crud->addColumn([
            'name'  => 'from',
            'type'  => 'show',
            'label' => 'Из',
        ]);

        $this->crud->addColumn([
            'name'  => 'to',
            'type'  => 'show',
            'label' => 'В',
        ]);

        $this->crud->addColumn([
            'name'  => 'receive_count',
            'label' => 'Вводимя сумма',
        ]);

        $this->crud->addColumn([
            'name'  => 'send_count',
            'label' => 'Выводимая сумма',
        ]);

        $this->crud->addColumn([
            'name'  => 'disply_status',
            'label' => 'Статус',
        ]);

        $this->crud->addColumn([
            'name'  => 'profit',
            'label' => 'Доход',
        ]);

        $this->crud->addColumn([
            'name'  => 'created_at',
            'label' => 'Создан',
        ]);

        $this->crud->addButtonFromModelFunction('line', 'fail_exchange', 'getFailButton');
        $this->crud->orderBy('created_at', 'desc');
    }

    public function update(UpdateRequest $request) {
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }

    public function store(StoreRequest $request) {
        $redirect_location = parent::storeCrud($request);
        return $redirect_location;
    }

    public function failExchange(Exchange $exchange) {
        $exchange->status = Exchange::DISCARD_BY_ADMIN;
        $exchange->save();

        return back();
    }
}
