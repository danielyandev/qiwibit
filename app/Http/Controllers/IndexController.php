<?php

namespace App\Http\Controllers;

use App\BitcoinApi;
use App\Models\QiwiWallet;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller {

    /**
     * Главная страница
     *
     * @return $this
     */
    public function index() {

        $js_data = [
            'rate_btc'          => config('settings.wallet.bitcoin.rate'),
            'rate_qiwi'         => config('settings.wallet.qiwi.rate'),
            'surcharge_bitcoin' => config('settings.wallet.bitcoin.surcharge'),
            'surcharge_qiwi'    => config('settings.wallet.qiwi.surcharge'),
        ];

        $free_qiwi = QiwiWallet::valid()->get()->sum('available_balance');

        $free_btc = BitcoinApi::getbalance()->response['result'] ?? '~~ERR';

        return view('index')->with(compact('js_data', 'free_qiwi', 'free_btc'));
    }

    /**
     * Показывает информацию о запросе.
     * Нужен для дебага.
     *
     * @param Request $request
     * @return null|string
     */
    public function dd(Request $request) {
        if (!config('app.debug'))
            return $this->notSupported();
        dd(func_get_args());
        return null;
    }

    /**
     * Показывает сообщение о том, что данное действие является неподдерживаемым, или находится в разработке.
     *
     * @return string
     */
    public function notSupported() {
        return config('app.debug') ? '--WIP--' : 'NotSupported';
    }
}
