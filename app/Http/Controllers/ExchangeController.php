<?php

namespace App\Http\Controllers;

use App\Mail\ExchangeCreatedMail;
use App\Models\Exchange;
use App\Models\ExchangeTargets\TargetModel;
use Illuminate\Http\Request;
use Log;
use Mail;
use Redirect;

class ExchangeController extends Controller {

    /**
     * Проверяет, создаёт обмен, а затем перенаправляет пользователя на нужную страницу.
     *
     * @param Request $request
     * @param $from
     * @param $to
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function applyExchange(Request $request, $from, $to) {

        $out = $from == 'bitcoin' ?
            $request->get('receive_count') * config('settings.wallet.bitcoin.rate') :
            $request->get('receive_count') / config('settings.wallet.qiwi.rate');

        $surcharge = config("settings.wallet.$from.surcharge");

        $request->offsetSet('send_count', $out * (1 - ($surcharge / 100)));
        $request->offsetSet('profit', $out * ($surcharge / 100));

        $from_target_name = 'App\Models\ExchangeTargets\\' . title_case($from) . 'Target';
        $to_target_name = 'App\Models\ExchangeTargets\\' . title_case($to) . 'Target';

        /** @var TargetModel $from_target */
        $from_target = new $from_target_name($request->all() + ['direction' => 'from']);
        /** @var TargetModel $to_target */
        $to_target = new $to_target_name($request->all() + ['direction' => 'to']);

        $exchange = new Exchange($request->all());

        $from_target->validate();
        $to_target->validate();
        $exchange->validate();

        $from_target->save();
        $to_target->save();

        $exchange->from_id = $from_target->id;
        $exchange->from_type = $from_target->getMorphClass();
        $exchange->to_id = $to_target->id;
        $exchange->to_type = $to_target->getMorphClass();

        $exchange->save();

        Mail::to($request->get('email'))->send(new ExchangeCreatedMail($exchange));

        Log::info('Created exchange ' . $exchange->toJson() . ' from ' . $request->ip());

        return Redirect::to($exchange->pay_redirect);
    }

    /**
     * Показывает страницу проверки статуса обмена.
     *
     * @param Request $request
     * @param int $id
     * @param string $email
     * @return $this
     */
    public function verifyExchange(Request $request, $id, $email) {
        $exchange = Exchange::where(compact('email', 'id'))->firstOrFail();
        /** @var TargetModel $from */
        $from = $exchange->from;

        $view = 'client.verify-exchange.' . kebab_case(class_basename($from->getMorphClass()));

        if (!view()->exists($view)) $view = 'client.verify-exchange.base';

        return view($view)->with(compact('exchange'));
    }

    public function discardExchange(Request $request, $id, $email) {
        $exchange = Exchange::where(compact('email', 'id'))->firstOrFail();
        $exchange->status = Exchange::DISCARD_BY_USER;
        $exchange->save();
        return back();
    }
}
