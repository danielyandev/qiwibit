<?php

namespace App\Http\Controllers;

use App\Models\Exchange;
use App\Models\WalletState;
use Carbon\Carbon;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Validator;

class ApiController extends Controller {

    /**
     * Отдаёт состояние кошельков на данный момент.
     *
     * @return array
     */
    public function getWalletState() {

        $qiwi = WalletState::where('wallet', 'qiwi')->latest()->first()->value ?? 0;
        $bitcoin = WalletState::where('wallet', 'bitcoin')->latest()->first()->value ?? 0;

        return compact('qiwi', 'bitcoin');
    }

    /**
     * Изменяет конфигурацию на сервере.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setSettings(Request $request) {

        $v = Validator::make($request->all(), ['settings' => 'required|array']);

        if ($v->fails()) return JsonResponse::create(['error' => true, 'messages' => $v->failed()]);

        $success = [];

        foreach ($request->get('settings') as $key => $value) {
            if (is_float($value)) $value = floatval($value);
            DB::table('settings')->where(compact('key'))->update(compact('value'));
            $success[] = $key;
        }

        return JsonResponse::create(['success' => true, 'fields' => $success]);
    }

    /**
     * Отдаёт статистику обменов.
     *
     * @return array
     */
    public function getToBtcStatistics() {
        $today = [
            'all'      => Exchange::toBitcoin()
                ->where('created_at', '>', Carbon::now()->subDay())->count(),
            'approved' => Exchange::toBitcoin()->accepted()
                ->where('created_at', '>', Carbon::now()->subDay())->count(),
            'error'    => Exchange::toBitcoin()->errored()
                ->where('created_at', '>', Carbon::now()->subDay())->count(),
        ];

        $week = [
            'all'      => Exchange::toBitcoin()
                ->where('created_at', '>', Carbon::now()->subWeek())->count(),
            'approved' => Exchange::toBitcoin()->accepted()
                ->where('created_at', '>', Carbon::now()->subWeek())->count(),
            'error'    => Exchange::toBitcoin()->errored()
                ->where('created_at', '>', Carbon::now()->subWeek())->count(),
        ];

        $year = [
            'all'      => Exchange::toBitcoin()
                ->where('created_at', '>', Carbon::now()->subYear())->count(),
            'approved' => Exchange::toBitcoin()->accepted()
                ->where('created_at', '>', Carbon::now()->subYear())->count(),
            'error'    => Exchange::toBitcoin()->errored()
                ->where('created_at', '>', Carbon::now()->subYear())->count(),
        ];

        return compact('today', 'week', 'year');
    }
}
