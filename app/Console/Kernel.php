<?php

namespace App\Console;

use App\Console\Commands\CollectWalletState;
use App\Console\Commands\ProcessExchanges;
use App\Console\Commands\UpdateQiwiBallance;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ProcessExchanges::class,
        CollectWalletState::class,
        UpdateQiwiBallance::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command('blockchain:process')->everyFiveMinutes();
        $schedule->command('blockchain:wallet:statistics')->daily();
        $schedule->command('blockchain:qiwi:balance-update')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
