<?php

namespace App\Console\Commands;

use App\Models\Exchange;
use App\Models\QiwiWallet;
use Exception;
use Illuminate\Console\Command;
use Log;

class ProcessExchanges extends Command {

    protected $signature = 'blockchain:process';

    protected $description = 'Process all opened exchanges';

    public function __construct() {
        parent::__construct();
    }

    /**
     * Выполняем оплату необходисых обменов
     */
    public function handle() {

        $exchanges = Exchange::with('from', 'to')->where('status', Exchange::CREATED)->get();

        $this->comment('Found ' . $exchanges->count() . ' exchanges.');
        $this->comment('Updating balances.');
        QiwiWallet::valid()->get()->map->updateBalance();

        foreach ($exchanges as $exchange)
            try {
                $id = $exchange->id;
                if ($exchange->checkExchange()) {
                    $msg = "Exchange '${id}' successfully checked. Sending payments ...";
                    $this->comment($msg);
                    Log::info($msg);
                    $exchange->endExchange();
                    $exchange->status = Exchange::DONE;
                    $exchange->save();
                } else if (now()->subDay(3)->greaterThan($exchange->created_at)) {
                    $exchange->status = Exchange::ERROR;
                    $exchange->save();
                }
            } catch (Exception $e) {
                $msg = "Exchange '${id}' failed. " . $e->getMessage();
                $this->error($msg);
                Log::error($msg, [$e]);
            }

        $this->comment('OK');
    }
}
