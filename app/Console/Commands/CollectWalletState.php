<?php

namespace App\Console\Commands;

use App\BitcoinApi;
use App\Models\QiwiWallet;
use App\Models\WalletState;
use Exception;
use Illuminate\Console\Command;
use Log;

class CollectWalletState extends Command {

    protected $signature = 'blockchain:wallet:statistics';
    protected $description = 'Update wallet value';

    public function __construct() {
        parent::__construct();
    }

    /**
     * Обновляем данные о счетах
     */
    public function handle() {

        try {

            WalletState::updateOrInsert(
                [
                    'wallet'     => 'qiwi',
                    'created_at' => now()->toDateString(),
                ],
                [
                    'value' => $amount = QiwiWallet::all()->sum('balance'),
                ]);

            $this->comment("QIWI: $amount");

        } catch (Exception $e) {
            $this->alert('Getting qiwi data fail.');
            $this->warn($e);
            $this->warn('');
            Log::error('Getting qiwi data fail.', [$e]);
        }

        try {

            $btc = BitcoinApi::getbalance();

            if ($btc->error) throw new Exception($btc->error);
            else $btc = $btc->response['result'];

            WalletState::updateOrInsert(
                [
                    'wallet'     => 'bitcoin',
                    'created_at' => now()->toDateString(),
                ],
                [
                    'value' => $btc,
                ]);

            $this->comment("BTC: $btc");

        } catch (Exception $e) {
            $this->alert('Getting bitcoin data fail.');
            $this->warn($e);
            $this->warn('');
            Log::error('Getting bitcoin data fail.', [$e]);
        }
    }
}
