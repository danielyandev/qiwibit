<?php

namespace App\Console\Commands;

use App\Models\QiwiWallet;
use Illuminate\Console\Command;

class UpdateQiwiBallance extends Command {

    protected $signature = 'blockchain:qiwi:balance-update';
    protected $description = 'Update qiwi ballacnes';


    public function handle() {
        QiwiWallet::all()->map->updateBalance();
    }
}
