<?php

namespace App\QiwiParser;

use App\Models\QiwiTransaction;
use App\Models\QiwiWallet;
use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\RequestOptions;
use InvalidArgumentException;
use PHPHtmlParser\Dom;

class QiwiWalletManager {

    const api_post_headers = [
        'Accept'          => 'application/json',
        'Content-Type'    => 'application/json',
        'client-software' => 'WEB v4.26.0',
    ];

    const json_post_headers = [
        'Referer'      => 'https://qiwi.com/main.action',
        'Accept'       => 'application/vnd.qiwi.sso-v1+json',
        'Content-Type' => 'application/json; charset=UTF-8',
    ];

    const form_post_headers = [
        'Referer'      => 'https://qiwi.com/main.action',
        'Accept'       => 'application/json',
        'Content-Type' => 'application/x-www-form-urlencoded',
    ];

    private $client; // GuzzleHttp клиент с нужными куки
    private $model; // модель кошелька
    private $cookie; // куки от кошелька
    private $oauth; // oauth ключ для апи киви

    /**
     * Создаём парсер киви из модели кошелька и сразу выптаемся залогиниться.
     * QiwiWalletManager constructor.
     * @param QiwiWallet $wallet
     */
    public function __construct(QiwiWallet $wallet) {
        $this->model = $wallet;

        /** @var CookieJar $cookie */
        list($this->cookie, $this->oauth) = Cache::get('ge.qiwi.wallet-' . $this->model->login . '.session', function () {
            return [[], ''];
        });

        $this->cookie = $cookie = new CookieJar(false, $this->cookie ?? []);

        $this->client = new Client(['headers' => $this->getDefaultHeaders(), 'cookies' => $this->cookie]);

        Cache::remember('ge.qiwi.wallet-' . $this->model->login . '.session', 60, function () use ($cookie) {
            $oauth = $this->login($this->model->login, $this->model->password);
            if (!$oauth) throw new InvalidArgumentException('Invalid qiwi wallet id:' . $this->model->id);
            return [$cookie->toArray(), $oauth];
        });

    }

    private function getDefaultHeaders() {
        return [
            'User-Agent'       => 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',
            'Accept-Language'  => 'ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'Accept-Encoding'  => 'gzip, deflate',
            'X-Requested-With' => 'XMLHttpRequest',
        ];
    }

    /**
     * Логинит кошелёк.
     * Вызывается само при создании экземпляра.
     * @param $login
     * @param $password
     * @return null|string
     */
    public function login($login, $password) {
        $json_post_headers = self::json_post_headers;
        $form_post_headers = self::form_post_headers;

        $wallet_creds = compact('login', 'password');

        $tgts = $this->client->post('https://sso.qiwi.com/cas/tgts', [
            RequestOptions::HEADERS => $json_post_headers,
            RequestOptions::JSON    => $wallet_creds,
        ]);

        $tgts_response = json_decode($tgts->getBody());
        $tgts_token = $tgts_response->entity->ticket; // TGT-478174-n0DV29UifOf7Rbfm9ebB1EbyAbTyqeQIPblKeDool5O56flczG

        $sts = $this->client->post('https://sso.qiwi.com/cas/sts', [
            RequestOptions::HEADERS => $json_post_headers,
            RequestOptions::JSON    => ['ticket' => $tgts_token, 'service' => 'https://qiwi.com/j_spring_cas_security_check'],
        ]);

        $sts_response = json_decode($sts->getBody());
        $sts_token = $sts_response->entity->ticket; // ST-6543937-0bKmbzgsacAFNXtQB9Iy

        $sts2 = $this->client->post('https://sso.qiwi.com/cas/sts', [
            RequestOptions::HEADERS => $json_post_headers,
            RequestOptions::JSON    => ['ticket' => $tgts_token, 'service' => 'https://qiwi.com/j_spring_cas_security_check'],
        ]);

        $sts2_response = json_decode($sts2->getBody());
        $sts2_token = $sts2_response->entity->ticket; // ST-6543937-0bKmbzgsacAFNXtQB9Iy

        $oauth = $this->client->post('https://qiwi.com/oauth/token', [
            RequestOptions::HEADERS     => $form_post_headers,
            RequestOptions::FORM_PARAMS => [
                'grant_type'      => 'sso_service_ticket',
                'client_id'       => 'sso.qiwi.com',
                'client_software' => 'WEB v4.26.0',
                'service_name'    => 'https://qiwi.com/j_spring_cas_security_check',
                'ticket'          => $sts_token,
            ],
        ]);

        $oauth_response = json_decode($oauth->getBody());
        $oauth_token = $oauth_response->access_token; // d35ac66719141d50

        $this->cookie->setCookie(new SetCookie([
            'Name'  => 'authorization',
            'Value' => 'TokenHeadV2 ' . base64_encode('sso.qiwi.com:' . $oauth_token),
        ]));

        $security = $this->client->post('https://qiwi.com/j_spring_cas_security_check', [
            RequestOptions::HEADERS     => $form_post_headers,
            RequestOptions::FORM_PARAMS => ['ticket' => $sts2_token],
        ]);

        $security_response = json_decode($security->getBody());

        return $security_response->code->_name == 'NORMAL' ?
            ($this->oauth = 'TokenHeadV2 ' . base64_encode('sso.qiwi.com:' . $oauth_token)) :
            null;
    }

    /**
     * @return float|null
     */
    public function getBallance() {
        $data = $this->getParsed('https://qiwi.com/settings.action');
        $node = $data->find('.account_current_amount')[0]->innerHtml;
        $match = [];
        preg_match('/(?<amount>\d+(?:[\.,])?\d+)/', $node, $match);
        $amount = str_replace(',', '.', $match['amount']) ?? null;
        return $amount === null ? null : floatval($amount);
    }

    private function getParsed($url) {
        $response = $this->client->get($url);
        $body = $response->getBody();
        $dom = new Dom();
        return $dom->load($body);
    }

    public function history($start_date, $end_date) {

        $start_date = Carbon::parse($start_date)->toAtomString();
        $end_date = Carbon::parse($end_date)->toAtomString();
        $person = str_replace('+', '', $this->model->login);

        $response = $this->client->get("https://edge.qiwi.com/payment-history/v2/persons/$person/payments", [
            RequestOptions::QUERY   => [
                'rows'      => 50,
                'startDate' => $start_date,
                'endDate'   => $end_date,
            ],
            RequestOptions::HEADERS => self::api_post_headers + ['authorization' => $this->oauth],
        ]);

        return json_decode($response->getBody(), true);
    }

    public function pay($id, $sum, $reciever, $comment = '') {

        $data = [
            "id"            => "$id",
            "sum"           => [
                "amount"   => $sum,
                "currency" => "643",
            ],
            "paymentMethod" => [
                "accountId" => "643",
                "type"      => "Account",
            ],
            "comment"       => $comment,
            "fields"        => [
                "sinap-form-version" => "qw::99, 12",
                "account"            => $reciever,
            ],
        ];


        $response = $this->client->post('https://edge.qiwi.com/sinap/api/v2/terms/99/payments', [
            RequestOptions::BODY    => json_encode($data),
            RequestOptions::HEADERS => self::api_post_headers + ['authorization' => $this->oauth],
        ]);

        logger($response->getBody()->__toString());

        QiwiTransaction::create([
            'qiwi_wallet_id' => $this->model->id,
            'value'          => $sum,
        ]);
    }
}