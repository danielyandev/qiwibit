<?php

namespace App\QiwiParser;

use App\Models\QiwiWallet;
use Artisan;

/**
 * Класс менеджера кошельков.
 * Вынесесено намеренно ибо в таком виде его легче расширять.
 * Class Qiwi
 * @package App\QiwiParser
 */
class Qiwi {

    public function massSend($sum, $reciever, $comment = '') {
        $wallets = QiwiWallet::valid()->get();

        foreach ($wallets as $wallet) {
            if ($sum == 0) continue;

            $available = $wallet->available_balance;

            if ($available > 0) {

                /** @var QiwiWalletManager $manager */
                $manager = $wallet->getManager();

                if ($sum >= $available) {
                    $manager->pay(now()->timestamp, $available, $reciever, $comment);
                    $sum -= $available;
                } else {
                    $to_send = $sum;
                    $manager->pay(now()->timestamp, $to_send, $reciever, $comment);
                    $sum -= $to_send;
                }
            }
        }
    }

    public function freeWallet($value) {
        $wallets = QiwiWallet::valid()->get();

        $max = [0, null];

        foreach ($wallets as $wallet) {
            $available = $wallet->limit_max - $wallet->balance;

            if ($available >= $value)
                return $wallet;

            if ($max[0] < $available)
                $max = [$available, $wallet];
        }

        return $max[1] ?: QiwiWallet::first();
    }
}