<?php

namespace App;

use Illuminate\Support\Facades\Facade;

class BitcoinApi extends Facade {

    protected static function getFacadeAccessor() {
        return 'bitcoin-api';
    }
}