<?php

namespace App;

/**
 * Класс для общения с биткоин апи
 * Class Bitcoin
 * @package App
 */
class Bitcoin {
    private $username;
    private $password;
    private $proto;

    private $host;
    private $port;
    private $url;
    private $CACertificate;
    private $id = 0;

    public function __construct($username = null, $password = null, $host = 'localhost', $port = 8332, $url = null) {
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;
        $this->url = $url;

        $this->proto = 'http';
        $this->CACertificate = null;
    }

    public function setSSL($certificate = null) {
        $this->proto = 'https'; // force HTTPS
        $this->CACertificate = $certificate;
    }

    public function __call($method, $params) {
        $response = new BitcoinInfo;

        $params = array_values($params);

        $this->id++;

        $request = json_encode([
            'method' => $method,
            'params' => $params,
            'id'     => $this->id,
        ]);

        $curl = curl_init("{$this->proto}://{$this->host}:{$this->port}/{$this->url}");

        $options = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_HTTPHEADER     => ['Content-type: application/json'],
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $request,
        ];

        if ($this->username && $this->password) {
            $options[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
            $options[CURLOPT_USERPWD] = $this->username . ':' . $this->password;
        }

        if (ini_get('open_basedir')) {
            unset($options[CURLOPT_FOLLOWLOCATION]);
        }

        if ($this->proto == 'https') {
            if (!empty($this->CACertificate)) {
                $options[CURLOPT_CAINFO] = $this->CACertificate;
                $options[CURLOPT_CAPATH] = DIRNAME($this->CACertificate);
            } else {
                $options[CURLOPT_SSL_VERIFYPEER] = false;
            }
        }

        curl_setopt_array($curl, $options);

        $response->raw_response = curl_exec($curl);
        $response->response = json_decode($response->raw_response, true);

        $response->status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $curl_error = curl_error($curl);

        curl_close($curl);

        if (!empty($curl_error)) {
            $response->error = $curl_error;
        }

        if ($response->response['error']) {
            $response->error = $response->response['error']['message'];
        } else if ($response->status != 200) {
            switch ($response->status) {
                case 400:
                    $response->error = 'HTTP_BAD_REQUEST';
                    break;
                case 401:
                    $response->error = 'HTTP_UNAUTHORIZED';
                    break;
                case 403:
                    $response->error = 'HTTP_FORBIDDEN';
                    break;
                case 404:
                    $response->error = 'HTTP_NOT_FOUND';
                    break;
            }
        }

        return $response;
    }
}

class BitcoinInfo {
    public $status = null;
    public $error = null;
    public $raw_response = null;
    public $response = null;

    public function result() {
        return $this->response['result'] ?? (is_array($this->response) ? $this->response : false);
    }

    public function __toString() {
        return $this->raw_response ?? '{"error": "EMPTY DATA"}';
    }
}
