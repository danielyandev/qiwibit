<?php

namespace App\Models;

use App\Models\ExchangeTargets\BitcoinTarget;
use App\Models\ExchangeTargets\TargetModel;
use Backpack\CRUD\CrudTrait;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Log;

/**
 * Модель обменов.
 * Здесь хранится необходимая информация для проверки и отправки платежей.
 *
 * Class Exchange
 * @property int status статус обмена
 * @property string email почта пользователя
 * @property TargetModel from
 * @property TargetModel to
 * @package App\Models
 */
class Exchange extends Model {
    use CrudTrait;

    const CREATED = 1; // обмен создан
    const DONE = 2; // обмен завершён успешно
    const ERROR = 3; // обмен завершён с ошибкой
    const DISCARD_BY_USER = 4; // обмен отклонён пользователем
    const DISCARD_BY_ADMIN = 5; // обмен отклонён администрацией

    protected $fillable = [
        'email', 'status', 'profit',
        'from_type', 'from_id',
        'to_type', 'to_id',
    ];

    protected $rules = [
        'email' => 'required|email',
    ];

    protected $rulesMessages = [
        'email.*' => 'Введите ваш реальный почтовый адрес',
    ];

    public function scopeToBitcoin(Builder $query) {
        return $query->where('to_type', BitcoinTarget::class);
    }

    public function scopeFromBitcoin(Builder $query) {
        return $query->where('from_type', BitcoinTarget::class);
    }

    public function scopeAccepted(Builder $query) {
        return $query->where('status', self::DONE);
    }

    public function scopeErrored(Builder $query) {
        return $query->whereIn('status', [self::ERROR, self::DISCARD_BY_ADMIN, self::DISCARD_BY_USER]);
    }

    public function scopeDiscarded(Builder $query) {
        return $query->whereIn('status', [self::DISCARD_BY_ADMIN, self::DISCARD_BY_USER]);
    }

    public function from() {
        return $this->morphTo('from');
    }

    public function to() {
        return $this->morphTo('to');
    }

    /**
     * Возвращает ссылку на которую будет перенесён клиент после создания обмена.
     * @return mixed|string
     */
    public function getPayRedirectAttribute() {
        return $this->from->getRedirectUrl();
    }

    /**
     * Врзвращает ссылку на страницу с проверкой статуса обмена.
     * @return string
     */
    public function getVerifyExchangeUrlAttribute() {
        // дабы нельзя было посмотреть чужой
        return route('exchange-verify', [
            'id'    => $this->id,
            'email' => $this->email,
        ]);
    }

    public function getDisplyStatusAttribute() {
        switch ($this->status) {
            case self::CREATED:
                return 'Заявка создана. Ожидание оплаты пользователя.';
            case self::DONE:
                return 'Обмен совершён.';
            case self::ERROR:
                return 'Обмен завершён с ошибкой.';
            case self::DISCARD_BY_USER:
                return 'Обмен отклонён пользователем.';
            case self::DISCARD_BY_ADMIN:
                return 'Обмен отклонён администрацией.';
            default:
                return '~~NOT FOUND STATE ' . $this->status;
        }
    }

    /**
     * Проверяет, оплатил ли юзер.
     *
     * @param null $context дополнительные данные
     * @return bool true если мы проверили платёжь (если юзер перевёл деньги)
     */
    public function checkExchange($context = null) {
        try {
            return $this->from->checkExchange($context);
        } catch (Exception $e) {
            Log::error('Check fails ' . $this->id);
            return false;
        }
    }

    public function getReceiveCountAttribute() {
        return $this->from->receive_count;
    }

    public function getSendCountAttribute() {
        return $this->to->send_count;
    }

    /**
     * Завершаем обмен и переводим юзеру его деньги.
     *
     * @param null $context дополнительные данные
     * @return mixed
     */
    public function endExchange($context = null) {
        try {
            $this->to->endExchange($context);
            $this->status = self::DONE;
            $this->save();

            return $this;
        } catch (Exception $e) {

            abort(404);
            return null;
        }
    }

    public function getFailButton() {
        if ($this->status != self::CREATED) return '';

        return '<a href="' . backpack_url('exchange/' . $this->id . '/fail') . '" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Закрыть обмен</a>';
    }
}
