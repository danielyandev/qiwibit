<?php

namespace App\Models;

use Jaspaul\EloquentModelValidation\Model as Base;

abstract class Model extends Base {

    protected $rules = [];
    protected $rulesMessages = [];

    protected function getRules(): array {
        return $this->rules ?: [];
    }

    protected function getMessages(): array {
        return $this->rulesMessages ?: parent::getMessages();
    }
}