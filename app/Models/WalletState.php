<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель для хранения состояния счёта на день.
 * Необходимо обновлять через schedule.
 *
 * Class WalletState
 * @package App\Models
 */
class WalletState extends Model {

    const UPDATED_AT = null;

    protected $fillable = ['wallet', 'value'];
}
