<?php

namespace App\Models\ExchangeTargets;

use App\Models\Exchange;
use App\Models\Model;
use Illuminate\Support\HtmlString;

abstract class TargetModel extends Model {

    public $label = '~~NO EXCHANGE LABEL';

    public function exchangeTo() {
        return $this->morphOne(Exchange::class, 'to');
    }

    public function exchangeFrom() {
        return $this->morphOne(Exchange::class, 'from');
    }

    public function exchange() {
        return $this->{'exchange' . studly_case($this->direction ?: 'from')}();
    }

    public function getRedirectUrl() {
        return $this->exchange->verify_exchange_url;
    }

    /**
     * Проверка обмена на оплаченность полхователем
     *
     * @param null $context
     * @return mixed
     */
    abstract public function checkExchange($context = null);

    /**
     * Завершает обмен
     *
     * @param null $context
     * @return mixed
     */
    abstract public function endExchange($context = null);

    /**
     * Возвращает шаблон c проверкой статуса обмена
     *
     * @return mixed
     */
    public function getVerifyFrom() {
        return new HtmlString('~~NO VERIFY FROM FOR ' . $this->label);
    }

    public function toHtml($content = '') {
        return new HtmlString($content);
    }
}