<?php

namespace App\Models\ExchangeTargets;

use App\Models\QiwiWallet;
use App\QiwiParser\Qiwi;
use App\QiwiParser\QiwiWalletManager;
use Cache;
use Carbon\Carbon;

class QiwiTarget extends TargetModel {

    protected $fillable = [
        'direction', 'user_qiwi_wallet', 'receive_qiwi_wallet',
        'send_count', 'receive_count',
    ];

    protected $rules = [
        'direction'           => 'required|string',
        'user_qiwi_wallet'    => 'nullable|regex:/\+?[78]\d{3}\d{7}/',
        'receive_qiwi_wallet' => 'nullable|numeric',
        'receive_count'       => 'nullable|numeric',
    ];

    protected $rulesMessages = [
        'user_qiwi_wallet.*' => 'Телефон заполнен не верно. (пример: +79261234567)',
        'receive_count.min'  => 'Слишком маленькая сумма (руб).',
        'send_count.max'     => 'Слишком большая сумма (руб).',
        'receive_count.*'    => 'Значение должно быть числом.',
    ];

    public function checkExchange($context = null) {
        $wallet = QiwiWallet::where('login', $this->receive_qiwi_wallet)->first();
        /** @var QiwiWalletManager $manager */
        $manager = $wallet->getManager();

        $from = substr($this->user_qiwi_wallet, 2);

        $qiwi_hist = Cache::remember('blockchain:qiwi:history-' . $wallet->id, 3, function () use ($manager) {
            return collect($manager->history($this->created_at, now())['data']);
        });

        $qiwi = $qiwi_hist->where('error', null)
            ->where('errorCode', 0)
            ->where('type', 'IN')
            ->where('status', 'SUCCESS')
            ->where('account', "+7$from")
            ->where('total.amount', $this->receive_count);

        return $qiwi->reduce(function ($carry, $item) {
            return $carry || Carbon::parse($item['date'])->greaterThan($this->created_at);
        }, false);
    }

    public function endExchange($context = null) {
        $sender = new Qiwi();
        $sender->massSend($this->send_count, $this->user_qiwi_wallet);
        return null;
    }


    /**
     * Возвращает подходящий киви кошелёк
     * @return string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getReceiveQiwiWalletAttribute() {
        if (!$this->getOriginal('receive_qiwi_wallet')) {
            $qiwi_manager = new Qiwi();
            $free_wallet = $qiwi_manager->freeWallet($this->from_count);
            if (!$free_wallet) return '~~NO_WALLET';
            $free = $free_wallet->login;
            $this->receive_qiwi_wallet = $free;
            $this->save();
        } else return $this->getOriginal('receive_qiwi_wallet');

        return $free;
    }

    public function getPayUrlAttribute() {
        $to = $this->exchange->to;
        $name = head(explode('-', kebab_case(class_basename($to->getMorphClass()))));
        return 'https://qiwi.com/payment/form/99?' . http_build_query([
                'currency'       => 643,
                'amountInteger'  => floor($this->receive_count),
                'amountFraction' => fmod($this->receive_count, 1) * 100,
                'extra'          => [
                    '\'account\'' => $this->receive_qiwi_wallet,
                    '\'comment\'' => 'Обмен с qiwi (' . $this->receive_count . ' RUB) на ' . $name . ' (' . $to->send_count . ') сервисом ' . config('app.name'),
                ],
            ]);
    }

    public function toHtml($content = '') {
        return parent::toHtml(view('admin.column.qiwi-target', ['target' => $this]));
    }

    protected function getRules(): array {
        $rules = $this->rules;

        if ($this->direction == 'to') {
            $max = QiwiWallet::valid()->sum('balance');

            $rules['user_qiwi_wallet'] = 'required|regex:/\+?[78]\d{3}\d{7}/';
            $rules['send_count'] = "required|numeric|max:$max";
        }

        if ($this->direction == 'from') {
            $min = config('settings.wallet.qiwi.min');

            $rules['user_qiwi_wallet'] = 'required|regex:/\+?[78]\d{3}\d{7}/';
            $rules['receive_count'] = "required|numeric|min:$min";
        }

        return $rules;
    }
}
