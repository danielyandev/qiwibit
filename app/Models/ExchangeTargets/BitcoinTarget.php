<?php

namespace App\Models\ExchangeTargets;

use App\BitcoinApi;
use App\BitcoinInfo;

class BitcoinTarget extends TargetModel {

    protected $fillable = [
        'direction', 'receive_bitcoin_wallet', 'user_bitcoin_wallet',
        'send_count', 'receive_count', 'end_transaction',
    ];

    protected $rules = [
        'direction'              => 'required|string',
        'receive_bitcoin_wallet' => 'nullable|regex:/[13][a-km-zA-HJ-NP-Z1-9]{25,34}/',
        'user_bitcoin_wallet'    => 'nullable|regex:/[13][a-km-zA-HJ-NP-Z1-9]{25,34}/',
        'send_count'             => 'nullable|numeric',
        'receive_count'          => 'nullable|numeric',
        'end_transaction'        => 'nullable|string',
    ];

    protected $rulesMessages = [
        'user_wallet.*'     => 'Счёт получателя заполнен не верно. (пример: 33fYWowZNLXTKWc5gGQzz16E66T3Pw22tt)',
        'receive_count.min' => 'Слишком маленькая сумма (btc).',
        'send_count.max'    => 'Слишком большая сумма (btc).',
        'receive_count.*'   => 'Значение должно быть числом.',
    ];

    public function getBitcoinAccountAttribute() {
        $id = $this->id;
        $exchange_id = $this->exchange->id;

        return "exchange-${exchange_id}_target-${id}";
    }

    public function creteReceiveWallet() {
        if ($this->receive_bitcoin_wallet && $this->receive_bitcoin_wallet != 'NULL')
            return $this->receive_bitcoin_wallet;

        $call = BitcoinApi::getnewaddress($this->bitcoin_account);
        $wallet = $call->result();

        $this->receive_bitcoin_wallet = $wallet;
        $this->save();

        return $wallet;
    }

    public function checkExchange($context = null) {
        $transactions = $this->getTransactions();

        $count = $transactions->sum(function ($transaction) {
            return $transaction['confirmations'] >= config('settings.wallet.bitcoin.confirmations') ?
                $transaction['amount'] : 0;
        });

        return $count >= $this->correctBticoinAmount($this->receive_count);
    }

    public function getTransactions() {
        $account = $this->bitcoin_account;
        if (!$account) return collect();

        $transactions = collect(BitcoinApi::listreceivedbyaddress()->result());

        return $transactions->filter(function ($transaction) use ($account) {
            return $transaction['account'] == $account;
        });
    }

    public function correctBticoinAmount($amount) {
        return round(floatval($amount) * 100000000) / 100000000;
    }

    public function endExchange($context = null) {
        BitcoinApi::settxfee(floatval(config('settings.wallet.bitcoin.fee')));

        $exchange = BitcoinApi::sendtoaddress(
            $this->user_bitcoin_wallet,
            $this->correctBticoinAmount($this->send_count)
        );

        $this->end_transaction = $exchange->result();
        return $exchange;
    }

    public function toHtml($content = '') {
        return parent::toHtml(view('admin.column.bitcoin-target', ['target' => $this]));
    }

    protected function getRules(): array {
        $rules = $this->rules;

        if ($this->direction == 'from') {
            $min = config('settings.wallet.bitcoin.min');

            $rules['receive_count'] = "required|numeric|min:$min";
        }


        if ($this->direction == 'to') {
            /** @var BitcoinInfo $balance */
            $balance = BitcoinApi::getbalance();
            $max = $balance->error ? 999 : $balance->result();

            $rules['user_bitcoin_wallet'] = 'required|regex:/[13][a-km-zA-HJ-NP-Z1-9]{25,34}/';
            $rules['send_count'] = "required|numeric|max:$max";
        }

        return $rules;
    }
}
