<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель транзакций для каждого киви кошелька.
 * Class QiwiTransaction
 * @package App\Models
 */
class QiwiTransaction extends Model {

    const UPDATED_AT = null;

    protected $fillable = [
        'qiwi_wallet_id', 'value',
    ];

    /**
     * Сслка на кошелёк, выполнивший операцию.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet() {
        return $this->belongsTo(QiwiWallet::class, 'qiwi_wallet_id');
    }

}
