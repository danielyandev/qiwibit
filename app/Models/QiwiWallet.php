<?php

namespace App\Models;

use App\QiwiParser\QiwiWalletManager;
use Backpack\CRUD\CrudTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;

/**
 * Модель киви кошелька
 * Class QiwiWallet
 * @package App\Models
 */
class QiwiWallet extends Model {
    use CrudTrait;

    protected $fillable = [
        'login', 'password', 'limit_month', 'limit_max', 'balance', 'valid',
    ];

    private $manager;

    protected static function boot() {
        parent::boot();

        static::creating(function (QiwiWallet $wallet) {
            $wallet->updateBalance();
        });
    }

    public function updateBalance() {
        try {
            $manager = $this->getManager();
            $val = $manager->getBallance();
            $this->balance = is_float($val) ? floatval($val) : 0;
            $this->valid = true;
            $this->save();
        } catch (Exception $e) {
            $this->balance = 0;
            $this->valid = false;
            Log::error($e);
        }
    }

    /**
     * @return QiwiWalletManager Менеджер(парсер) кошелька
     */
    public function getManager() {
        if (!$this->manager)
            return $this->manager = new QiwiWalletManager($this);
        return $this->manager;
    }

    public function scopeValid($wallet) {
        return $wallet->where('valid', true);
    }

    /**
     * @return \Illuminate\Config\Repository|mixed лимит в месяц
     */
    public function getLimitMonthAttribute() {
        return $this->attributes['limit_month'] < 0 ? config('settings.wallet.qiwi.limits.per_month') : $this->attributes['limit_month'];
    }

    /**
     * @return \Illuminate\Config\Repository|mixed лимит максимального значения на счёте
     */
    public function getLimitMaxAttribute() {
        return $this->attributes['limit_max'] < 0 ? config('settings.wallet.qiwi.limits.max') : $this->attributes['limit_max'];
    }

    /**
     * @return float|int сколько было потрачено денег за этот месяц с этого кошелька
     */
    public function getStubTransactionMonthAttribute() {
        $per_mounth = $this->transactions()
            ->where(QiwiTransaction::CREATED_AT, '>=', now()->startOfMonth())
            ->sum('value');

        return is_float($per_mounth) ? $per_mounth : 0;
    }

    /**
     * Сснылка на все транзакции в кошельке
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions() {
        return $this->hasMany(QiwiTransaction::class, 'qiwi_wallet_id');
    }

    /**
     * @return int|mixed доступный для перевода баланс счёта
     */
    public function getAvailableBalanceAttribute() {
        $per_mounth = $this->stub_transaction_month;
        $limit = $this->limit_month - $per_mounth;
        if ($limit <= 0) return 0;
        $val = min($this->balance, $limit);
        return $val > 0 ? $val : 0;
    }

    public function getHistoryButton() {
        if (!$this->valid) return '';

        return '<a href="' . backpack_url('qiwi_wallets/' . $this->id . '/history') . '" class="btn btn-xs btn-default"><i class="fa fa-search"></i> История обменов</a>';
    }

    public function getSendButton() {
        if (!$this->valid) return '';

        return '<a href="' . backpack_url('qiwi_wallets/' . $this->id . '/send') . '" class="btn btn-xs btn-default"><i class="fa fa-arrow-circle-right"></i> Перевести</a>';
    }

    public function getRefreshButton() {
        if (!$this->valid) return '';

        return '<a href="' . backpack_url('qiwi_wallets/' . $this->id . '/refresh') . '" class="btn btn-xs btn-default"><i class="fa fa-refresh"></i></a>';
    }
}

QiwiWallet::creating(function (QiwiWallet $wallet) {
    if (($wallet->limit_month ?? -1) < 0)
        $wallet->limit_month = -1;

    if (($wallet->limit_max ?? -1) < 0)
        $wallet->limit_max = -1;

    if (!$wallet->balance)
        $wallet->updateBalance();
});
