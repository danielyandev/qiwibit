let mix = require('laravel-mix');

let ComponentFactory = require('laravel-mix/src/components/ComponentFactory');
new ComponentFactory().installAll();

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/admin.js', 'public/js')
    .copy('resources/assets/images', 'public/images', true)
    .stylus('resources/assets/stylus/app.styl', 'public/css')
    .stylus('resources/assets/stylus/admin.styl', 'public/css')
    .disableNotifications();

if (!mix.config.hmr)
    mix.version()
        .browserSync({
            notify: false,
            open: false,
            proxy: 'localhost',
        });

if (!mix.inProduction())
    mix.sourceMaps();

Mix.dispatch('init', mix);

let WebpackConfig = require('laravel-mix/src/builder/WebpackConfig');

module.exports = new WebpackConfig().build();