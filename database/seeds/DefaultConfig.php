<?php

use Illuminate\Database\Seeder;

class DefaultConfig extends Seeder {

    protected $settings = [
        [
            'key'         => 'wallet.qiwi.surcharge',
            'name'        => 'Киви надбавка',
            'description' => 'Надбавка, когда мы получаем киви рубли',
            'field'       => '{"name":"value","label":"Limit","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.qiwi.min',
            'name'        => 'Минимальное значение киви',
            'description' => 'Минимальное значение киви',
            'field'       => '{"name":"value","label":"Limit","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.qiwi.limits.per_month',
            'name'        => 'Киви максимум за месяц',
            'description' => 'Стандартное значение максимального значения переводов за месяц',
            'field'       => '{"name":"value","label":"Limit","type":"number"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.qiwi.limits.max',
            'name'        => 'Киви максимум',
            'description' => 'Стандартное значение максимального значения киви кошелька',
            'field'       => '{"name":"value","label":"Limit","type":"number"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.qiwi.rate',
            'description' => 'Курс rub->btc',
            'name'        => 'Курс rub->btc',
            'field'       => '{"name":"value","label":"Limit","type":"text"}',
            'active'      => 1,
        ],

        [
            'key'         => 'wallet.bitcoin.surcharge',
            'name'        => 'Надбавка биткоина',
            'description' => 'Надбавка, когда мы получаем биткоины',
            'field'       => '{"name":"value","label":"Limit","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.bitcoin.min',
            'name'        => 'Минимальное значение биткоина',
            'description' => 'Минимальное значение биткоина',
            'field'       => '{"name":"value","label":"Limit","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.bitcoin.fee',
            'name'        => 'Bitcoin fee',
            'description' => 'Bitcoin fee',
            'field'       => '{"name":"value","label":"Limit","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.bitcoin.rate',
            'description' => 'Курс btc->rub',
            'name'        => 'Курс btc->rub',
            'field'       => '{"name":"value","label":"Limit","type":"text"}',
            'active'      => 1,
        ],
        [
            'key'         => 'wallet.bitcoin.confirmations',
            'name'        => 'Подтверждейний для bitcoin сети',
            'description' => 'Подтверждейний для bitcoin сети для проверки оплаты пользователя',
            'field'       => '{"name":"value","label":"Сonfirmations","type":"text"}',
            'active'      => 1,
        ],
    ];

    public function run() {
        foreach ($this->settings as $index => $setting) {

            $state = DB::table('settings')->where('key', $setting['key'])->count();

            if ($state) {
                $this->command->info("Database already has " . $setting['key'] . ".");
                continue;
            }

            if (!isset($setting['value']))
                $setting['value'] = config('settings.' . $setting['key']);

            $result = DB::table('settings')->insert($setting);

            if (!$result) {
                $this->command->info("Insert failed at record $index.");
                return;
            }
        }

        $this->command->info('Inserted ' . count($this->settings) . ' records.');
    }
}
