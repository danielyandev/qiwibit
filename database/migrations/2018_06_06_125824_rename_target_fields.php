<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTargetFields extends Migration {

    public function up() {
        Schema::table('qiwi_targets', function (Blueprint $table) {
            $table->dropColumn('user_wallet');
            $table->string('user_qiwi_wallet')->nullable();

            $table->string('receive_qiwi_wallet')->nullable();
        });

        Schema::table('bitcoin_targets', function (Blueprint $table) {
            $table->dropColumn('receive_wallet');
            $table->string('receive_bitcoin_wallet')->nullable();

            $table->dropColumn('user_wallet');
            $table->string('user_bitcoin_wallet')->nullable();
        });
    }

    public function down() {
        Schema::table('qiwi_targets', function (Blueprint $table) {
            $table->dropColumn('user_qiwi_wallet');
            $table->string('user_wallet')->nullable();

            $table->dropColumn('receive_qiwi_wallet');
        });

        Schema::table('bitcoin_targets', function (Blueprint $table) {
            $table->dropColumn('receive_bitcoin_wallet');
            $table->string('receive_wallet')->nullable();

            $table->dropColumn('user_bitcoin_wallet');
            $table->string('user_wallet')->nullable();
        });
    }
}
