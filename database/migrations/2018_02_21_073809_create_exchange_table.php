<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchanges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('from');
            $table->string('from_wallet');
            $table->string('from_count');
            $table->string('to');
            $table->string('to_wallet');
            $table->string('to_count');
            $table->integer('status')->default(\App\Models\Exchange::CREATED);
            $table->string('extra')->default(serialize([]));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchanges');
    }
}
