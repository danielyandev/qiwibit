<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeparateExchangesByMorphs extends Migration {

    public function up() {
        DB::table('exchanges')->truncate();

        Schema::table('exchanges', function (Blueprint $table) {

            $table->dropColumn('from');
            $table->dropColumn('from_wallet');
            $table->dropColumn('from_count');
            $table->dropColumn('to');
            $table->dropColumn('to_wallet');
            $table->dropColumn('to_count');
            $table->dropColumn('extra');

            $table->morphs('from');
            $table->morphs('to');

        });
    }

    public function down() {
        Schema::table('exchanges', function (Blueprint $table) {
            $table->dropMorphs('from');
            $table->dropMorphs('to');

            $table->string('from');
            $table->string('from_wallet');
            $table->string('from_count');
            $table->string('to');
            $table->string('to_wallet');
            $table->string('to_count');
            $table->string('extra')->default(serialize([]));
        });
    }
}
