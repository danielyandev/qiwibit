<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQiwiTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qiwi_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('direction', 5);

            $table->string('user_wallet')->nullable();

            $table->double('send_count',8 * 3,8)->nullable();
            $table->double('receive_count',8 * 3,8)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qiwi_targets');
    }
}
