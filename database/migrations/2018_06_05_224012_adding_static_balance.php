<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddingStaticBalance extends Migration {

    public function up() {
        Schema::table('qiwi_wallets', function ($table) {
            $table->float('balance', 10, 3)->default(0);
            $table->boolean('valid');
        });
    }

    public function down() {
        Schema::table('qiwi_wallets', function (Blueprint $table) {
            $table->dropColumn('balance');
            $table->dropColumn('valid');
        });
    }
}
