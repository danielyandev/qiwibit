<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitcoinTargetsTable extends Migration {

    public function up() {
        Schema::create('bitcoin_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('direction', 5);

            $table->string('receive_wallet')->nullable();
            $table->string('user_wallet')->nullable();

            $table->double('send_count', 8 * 3, 8)->nullable();
            $table->double('receive_count', 8 * 3, 8)->nullable();

            $table->string('end_transaction')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('bitcoin_targets');
    }
}
